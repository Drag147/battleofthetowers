﻿using UnityEngine;

[SerializeField]
public class MoveCamera : MonoBehaviour
{
    [SerializeField] private float PanSpeed = 350f;
    [SerializeField] private float BoundsXLeft = 0f;
    [SerializeField] private float BoundsXRight;
    public Camera GameCamera;
    private Vector3 lastPanPosition;
    private int panFingerId;

    private float deltaFixScroll = -0.55f;
    private bool _enableScrollingMap;

    private void Awake()
    {
        if (GameCamera == null)
        {
            GameCamera = GetComponent<Camera>();
        }
    }

    public void ConfigureCameraBounds(float widthLevel, SidePlayer sidePlayer)
    {
        float screenAspect = Screen.width / (float)Screen.height;
        float camHalfHeight = GameCamera.orthographicSize;
        float camHalfWidth = screenAspect * camHalfHeight;
        float camWidth = 2.0f * camHalfWidth;
        BoundsXRight = widthLevel - camWidth + deltaFixScroll;

        _enableScrollingMap = BoundsXRight != deltaFixScroll;
        if (_enableScrollingMap && sidePlayer == SidePlayer.RIGHT)
        {
            Vector3 positionNow = transform.position;
            transform.position = new Vector3(BoundsXRight, positionNow.y, positionNow.z);
        }
    }

    void Update()
    {
        if (_enableScrollingMap)
        {
            if (Input.touchSupported && Application.platform != RuntimePlatform.WebGLPlayer)
            {
                HandleTouch();
            }
            else
            {
                HandleMouse();
            }
        }
    }

    void HandleTouch()
    {
        switch (Input.touchCount)
        {
            case 1:
                Touch touch = Input.GetTouch(0);
                if (touch.phase == TouchPhase.Began)
                {
                    lastPanPosition = touch.position;
                    panFingerId = touch.fingerId;
                }
                else if (touch.fingerId == panFingerId && touch.phase == TouchPhase.Moved)
                {
                    PanCamera(touch.position);
                }
                break;
            default:
                break;
        }
    }

    void HandleMouse()
    {
        if (Input.GetMouseButtonDown(0))
        {
            lastPanPosition = Input.mousePosition;
        }
        else if (Input.GetMouseButton(0))
        {
            PanCamera(Input.mousePosition);
        }
    }

    void PanCamera(Vector3 newPanPosition)
    {
        Vector3 offset = GameCamera.ScreenToViewportPoint(lastPanPosition - newPanPosition);
        Vector3 move = new Vector3(offset.x * PanSpeed, 0, 0);
        transform.Translate(move, Space.World);
        Vector3 pos = transform.position;
        pos.x = Mathf.Clamp(transform.position.x, BoundsXLeft, BoundsXRight);
        transform.position = pos;
        lastPanPosition = newPanPosition;
    }
}