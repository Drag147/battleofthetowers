﻿using System;
using UnityEngine;
using UnityEngine.UI;
using static ManaHandler;

public class ManaButtonUI : MonoBehaviour
{
    public Action clickOnManaAction;

    public Text nowValueText;
    public Text maxValueText;
    public Text imrpoveValueText;

    public void Awake()
    {
        GetComponent<Button>().onClick.AddListener(ClickManaForImprove);
    }

    public void ShowManaValues(object sender, ManaEventArgs manaEventArgs)
    {
        nowValueText.text = manaEventArgs.nowValue.ToString();
        maxValueText.text = manaEventArgs.maxValue.ToString();
        imrpoveValueText.text = "Улучш. за " + manaEventArgs.improveCost.ToString();
    }

    public void ClickManaForImprove()
    {
        clickOnManaAction?.Invoke();
    }
}
