﻿using UnityEngine;

public class ManaConfigurerSubscribes : MonoBehaviour
{
    public GameObject manaButtonUIPrefab;
    public RectTransform manaButtonUIPosition;

    public void SubscribeManaButtonUI(ManaHandler manaHandler)
    {
        ManaButtonUI manaButtonUI = Instantiate(manaButtonUIPrefab, manaButtonUIPosition).GetComponent<ManaButtonUI>();
        manaButtonUI.clickOnManaAction += manaHandler.ImproveMana;
        manaHandler.OnManaChange += manaButtonUI.ShowManaValues;
    }
}
