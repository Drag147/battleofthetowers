﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseController : MonoBehaviour
{
    public EventHandler<EventArgs> OnPause;
    public EventHandler<EventArgs> OnPlay;

    public GameObject pauseUI;
    public bool isPause = false;
    public Sprite pauseImage;
    public Sprite playImage;
    public Image playPauseButtonImage;

    public void ClickOnPlayPause()
    {
        isPause = !isPause;
        pauseUI.SetActive(isPause);
        playPauseButtonImage.sprite = isPause ? playImage : pauseImage;
        if (isPause)
        {
            OnPause?.Invoke(this, EventArgs.Empty);
        } else
        {
            OnPlay?.Invoke(this, EventArgs.Empty);
        }
    }

    public void ClickOnMainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }

    public void ClickOnQuit()
    {
        Application.Quit();
    }
}
