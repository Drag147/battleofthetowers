﻿using UnityEngine;
using UnityEngine.UI;

public class ImageAnimation : MonoBehaviour
{

	public Sprite[] sprites;
	public bool loop = true;
	public float upScaleCoeff = 3f;
	public bool destroyOnEnd = false;

	private int spritePerFrame;
	private int index = 0;
	private Image image;
	private int frame = 0;

	void Awake()
	{
		image = GetComponent<Image>();
		spritePerFrame = sprites.Length;
	}

	void Update()
	{
		if (!loop && index == sprites.Length) return;
		frame++;
		if (frame < spritePerFrame) return;
		image.sprite = sprites[index];
		((RectTransform)transform).sizeDelta = new Vector2(sprites[index].rect.width / (sprites[index].pixelsPerUnit / upScaleCoeff),
			sprites[index].rect.height / (sprites[index].pixelsPerUnit / upScaleCoeff));
		frame = 0;
		index++;
		if (index >= sprites.Length)
		{
			if (loop) index = 0;
			if (destroyOnEnd) Destroy(gameObject);
		}
	}
}