﻿using System.Collections.Generic;
using UnityEngine;
using static CastEntityHandler;

public class UnitConfigurerSubscribes : MonoBehaviour
{
    public GameObject buttonUnitUI;
    public RectTransform panelUnitsUI;

    public void SubscribeUnitsButtonUI(List<GameObject> unitSpawners, List<UnitInfo> unitInfos)
    {
        for(int i = 0; i < unitSpawners.Count; i++)
        {
            //получаем 2 компонента cast и culdown
            if (unitSpawners[i].TryGetComponent(out CastEntityHandler castHandler))
            {
                //Создаём UI часть как родителя панельки
                var buttonUI = Instantiate(buttonUnitUI, panelUnitsUI);
                buttonUI.GetComponent<CastAndCuldownUI>().imageIcon.sprite = CreationUnitSpawners.LoadSpriteByFileNameOrUnitName(unitInfos[i]);
                CastAndCuldownUI castAndCuldownUI = buttonUI.GetComponent<CastAndCuldownUI>();

                castAndCuldownUI.actionOnClickButton += castHandler.CastEntity;
                castHandler.OnShowCostCastEntity += (object sender, CastEntityEventArgs args) =>
                {
                    castHandler.OnEntityCastedCostCheck?.Invoke(castHandler, args);
                    args.manaNeededRight = args.manaRight;
                    castAndCuldownUI.ShowCostCast(args);
                };

                if (unitSpawners[i].TryGetComponent(out CuldownEntityHandler culdownHandler))
                {
                    culdownHandler.OnCuldownLeftChange += castAndCuldownUI.ShowCuldown;
                }
            }
        }
    }
}