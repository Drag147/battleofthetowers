﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class GameOverController : MonoBehaviour
{
    public Action OnRestart;
    public Action OnMainMenu;

    public GameObject endGame;
    public Text endText;

    public void ShowGameOverWindow(string textToShow)
    {
        endGame.SetActive(true);
        endText.text = textToShow;
    }

    public void RestartLevelClick()
    {
        OnRestart?.Invoke();
    }

    public void ToMainMenuClick()
    {
        OnMainMenu?.Invoke();
    }
}
