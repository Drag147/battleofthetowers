﻿using UnityEngine;
using static CastEntityHandler;

public class SpecialConfigurerSubscribes : MonoBehaviour
{
    public RectTransform specialButtonUIPosition;

    public void SubscribeSpecialButtonUI(AbstractSpecialSpawnHandler specialHandler)
    {
        var castHandler = specialHandler.GetComponent<CastEntityHandler>();
        var cullDownHandler = specialHandler.GetComponent<CuldownEntityHandler>();
        CastAndCuldownUI specialButtonUI = Instantiate(castHandler.prefabForUICast, specialButtonUIPosition).GetComponent<CastAndCuldownUI>();

        specialButtonUI.actionOnClickButton += castHandler.CastEntity;

        castHandler.OnShowCostCastEntity += (object sender, CastEntityEventArgs args) =>
        {
            castHandler.OnEntityCastedCostCheck?.Invoke(castHandler, args);
            args.manaNeededRight = args.manaRight;
            specialButtonUI.ShowCostCast(args);
        };

        cullDownHandler.OnCuldownLeftChange += specialButtonUI.ShowCuldown;
    }
}
