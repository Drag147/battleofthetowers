﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour {
    public readonly string ScaneNameForPlayWithBot = "PlayWithBot";

    private System.Random random = new System.Random();

    public Button startButton;
    public RectTransform panelPlayerArmy;
    public Image towerPlayerImage;
    public GameObject unitCellPrefab;
    public Dropdown dropdownDiffBot;
    public Dropdown dropdownWidthMap;
    public Dropdown dropdownSidePlayer;

    public SelectTowerGrid selectTowerGrid;
    public SelectUnitGrid selectUnitGrid;
    private List<GameObject> unitsOnUI = new List<GameObject>();

    private PlayerConfig playerConfig;

    private TowerInfo towerInfoPlayer;
    private UnitsInfoConfig playerUnitsConfig;

    private TowerInfo towerInfoBot;
    private UnitsInfoConfig botUnitsInfoConfig;

    public void LoadLastConfig()
    {
        dropdownSidePlayer.value = PlayerPrefs.GetInt(KeysForPlayerPrefs.KeyForSideForPlayer, 0);
        dropdownDiffBot.value = PlayerPrefs.GetInt(KeysForPlayerPrefs.KeyForDifficaltBot, 0);
        dropdownWidthMap.value = MapSizes.GetNumberByWidth(PlayerPrefs.GetFloat(KeysForPlayerPrefs.KeyForMapWidth, 0));
    }

    public void Awake()
    {
        LoadLastConfig();

        selectTowerGrid.OnClickTowerInGrid += SelectNewTower;
        selectUnitGrid.OnClickUnitInGrid += SelectNewUnit;

        playerConfig = PlayerConfig.GetDefaultPlayerConfig();

        if (!PlayerPrefs.HasKey(KeysForPlayerPrefs.KeyForPlayerConfig))
        {
            PlayerPrefs.SetString(KeysForPlayerPrefs.KeyForPlayerConfig, JsonUtility.ToJson(playerConfig));
            PlayerPrefs.Save();
        }

        towerInfoPlayer = AllTowerUtils.GetTowerByName(playerConfig.nameTower);
        

        towerInfoBot = AllTowerUtils.GetTowerByName("WoodTower");

        botUnitsInfoConfig = new UnitsInfoConfig { unitsUnfo = AllUnitsUtils.GetAllUnits() };

        towerPlayerImage.sprite = CommonLoadResourcesByFileName.LoadSpriteByFileName(towerInfoPlayer.fileNameForTowerSprite);

        ShowPlayerArmy();
    }

    public void ClickOnPlayWithBot()
    {
        ConfigurateTowersBeforePlaying((SidePlayer)dropdownSidePlayer.value);
        playerUnitsConfig = AllUnitsUtils.GetNowUnitsPlayer(playerConfig.selectedArmyPlayer);

        if ((SidePlayer)dropdownSidePlayer.value == SidePlayer.LEFT)
        {
            PlayerPrefs.SetString(KeysForPlayerPrefs.KeyForLeftTowerInfo, JsonUtility.ToJson(towerInfoPlayer));
            PlayerPrefs.SetString(KeysForPlayerPrefs.KeyForRightTowerInfo, JsonUtility.ToJson(towerInfoBot));

            PlayerPrefs.SetString(KeysForPlayerPrefs.KeyForLeftUnitsInfoPlayer, JsonUtility.ToJson(playerUnitsConfig));
            PlayerPrefs.SetString(KeysForPlayerPrefs.KeyForRightUnitsInfoPlayer, JsonUtility.ToJson(botUnitsInfoConfig));
        } 
        else
        {
            PlayerPrefs.SetString(KeysForPlayerPrefs.KeyForLeftTowerInfo, JsonUtility.ToJson(towerInfoBot));
            PlayerPrefs.SetString(KeysForPlayerPrefs.KeyForRightTowerInfo, JsonUtility.ToJson(towerInfoPlayer));

            PlayerPrefs.SetString(KeysForPlayerPrefs.KeyForLeftUnitsInfoPlayer, JsonUtility.ToJson(botUnitsInfoConfig));
            PlayerPrefs.SetString(KeysForPlayerPrefs.KeyForRightUnitsInfoPlayer, JsonUtility.ToJson(playerUnitsConfig));
        }

        //Выбираем случайную карту
        PlayerPrefs.SetString(KeysForPlayerPrefs.KeyForMapBackgroundName, "Image/Background/" + random.Next(1, 5));
        PlayerPrefs.SetFloat(KeysForPlayerPrefs.KeyForMapWidth, MapSizes.GetWidthByNumber(dropdownWidthMap.value));

        PlayerPrefs.SetInt(KeysForPlayerPrefs.KeyForDifficaltBot, dropdownDiffBot.value);

        PlayerPrefs.SetInt(KeysForPlayerPrefs.KeyForSideForPlayer, dropdownSidePlayer.value);

        PlayerPrefs.Save();
        
        SceneManager.LoadScene(ScaneNameForPlayWithBot);
    }

    public void ConfigurateTowersBeforePlaying(SidePlayer sidePlayer)
    {
        int layerLeftPlayer = LayerMask.NameToLayer("LeftPlayer");
        int layerRightPlayer = LayerMask.NameToLayer("RightPlayer");

        string tagLeftPlayer = "LeftPlayer";
        string tagRightPlayer = "RightPlayer";

        switch (sidePlayer)
        {
            case SidePlayer.LEFT:
                towerInfoPlayer.layerAllias = layerLeftPlayer;
                towerInfoPlayer.tagAllias = tagLeftPlayer;
                towerInfoPlayer.tagEnemy = tagRightPlayer;
                towerInfoPlayer.layerEnemy = layerRightPlayer;
                towerInfoPlayer.typePlayer = TypePlayer.PLAYER;

                towerInfoBot.layerAllias = layerRightPlayer;
                towerInfoBot.tagAllias = tagRightPlayer;
                towerInfoBot.tagEnemy = tagLeftPlayer;
                towerInfoBot.layerEnemy = layerLeftPlayer;
                towerInfoBot.typePlayer = TypePlayer.BOT;
                break;
            case SidePlayer.RIGHT:
                towerInfoBot.layerAllias = layerLeftPlayer;
                towerInfoBot.tagAllias = tagLeftPlayer;
                towerInfoBot.tagEnemy = tagRightPlayer;
                towerInfoBot.layerEnemy = layerRightPlayer;
                towerInfoBot.typePlayer = TypePlayer.BOT;

                towerInfoPlayer.layerAllias = layerRightPlayer;
                towerInfoPlayer.tagAllias = tagRightPlayer;
                towerInfoPlayer.tagEnemy = tagLeftPlayer;
                towerInfoPlayer.layerEnemy = layerLeftPlayer;
                towerInfoPlayer.typePlayer = TypePlayer.PLAYER;
                break;
            default:
                throw new ArgumentException("Unsupported side player: " + sidePlayer);
        }
    }

    public void SelectNewTower(string name)
    {
        playerConfig.nameTower = name;
        towerInfoPlayer = AllTowerUtils.GetTowerByName(name);
        towerPlayerImage.sprite = CommonLoadResourcesByFileName.LoadSpriteByFileName(AllTowerUtils.GetTowerByName(name).fileNameForTowerSprite);
        PlayerPrefs.SetString(KeysForPlayerPrefs.KeyForPlayerConfig, JsonUtility.ToJson(playerConfig));
        PlayerPrefs.Save();
        selectTowerGrid.ClickOnClose();
    }

    public void SelectNewUnit(int indexUnit, string name)
    {
        if (name == null || name.Length == 0)
        {
            playerConfig.selectedArmyPlayer.RemoveAt(indexUnit);
            unitsOnUI[indexUnit].GetComponent<Image>().sprite = null;
        }
        else
        {
            if (indexUnit >= playerConfig.selectedArmyPlayer.Count)
            {
                playerConfig.selectedArmyPlayer.Add(name);
                unitsOnUI[playerConfig.selectedArmyPlayer.Count - 1].GetComponent<Image>().sprite = CommonLoadResourcesByFileName.LoadSpriteByFileName(AllUnitsUtils.GetUnitByName(name).fileNameImageIcon);
            }
            else
            {
                playerConfig.selectedArmyPlayer[indexUnit] = name;
                unitsOnUI[indexUnit].GetComponent<Image>().sprite = CommonLoadResourcesByFileName.LoadSpriteByFileName(AllUnitsUtils.GetUnitByName(name).fileNameImageIcon);
            }
        }
        if (playerConfig.selectedArmyPlayer.Count == 0)
        {
            startButton.interactable = false;
        } 
        else
        {
            startButton.interactable = true;
        }
        PlayerPrefs.SetString(KeysForPlayerPrefs.KeyForPlayerConfig, JsonUtility.ToJson(playerConfig));
        PlayerPrefs.Save();
        ShowPlayerArmy();
        selectUnitGrid.ClickOnClose();
    }

    public void ShowPlayerArmy()
    {
        foreach (var unitOnUI in unitsOnUI)
        {
            Destroy(unitOnUI);
        }
        unitsOnUI.Clear();

        playerUnitsConfig = AllUnitsUtils.GetNowUnitsPlayer(playerConfig.selectedArmyPlayer);

        for (int i = 0; i < playerUnitsConfig.unitsUnfo.Count; i++)
        {
            GameObject unitOnUI = Instantiate(unitCellPrefab, panelPlayerArmy);
            unitOnUI.GetComponent<Image>().sprite = CommonLoadResourcesByFileName.LoadSpriteByFileName(playerUnitsConfig.unitsUnfo[i].fileNameImageIcon);

            Button button = unitOnUI.GetComponent<Button>();
            int tmpI = i;
            button.onClick.AddListener(() =>
            {
                selectUnitGrid.ClickOnUnitInMenu(tmpI, true);
            });

            unitsOnUI.Add(unitOnUI);
        }
        for (int i = playerUnitsConfig.unitsUnfo.Count; i < 4; i++)
        {
            GameObject unitOnUI = Instantiate(unitCellPrefab, panelPlayerArmy);
            unitOnUI.GetComponent<Image>().sprite = null;

            Button button = unitOnUI.GetComponent<Button>();
            int tmpI = i;
            button.onClick.AddListener(() =>
            {
                selectUnitGrid.ClickOnUnitInMenu(tmpI, false);
            });

            unitsOnUI.Add(unitOnUI);
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
