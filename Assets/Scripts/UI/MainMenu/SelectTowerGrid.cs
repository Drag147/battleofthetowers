﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectTowerGrid : MonoBehaviour
{
    public Action<string> OnClickTowerInGrid;

    public GameObject panelSelectTower;
    public GameObject cellTowerPrefab;
    public Transform contentTransform;

    private List<GameObject> caheAddedTowers = new List<GameObject>();

    public void ClickOnSelectTower()
    {
        panelSelectTower.SetActive(true);

        if (caheAddedTowers.Count == 0)
        {
            foreach (var tower in AllTowerUtils.allTowers.Values)
            {
                GameObject cellTower = Instantiate(cellTowerPrefab, contentTransform);
                Image image = cellTower.GetComponent<Image>();
                image.sprite = CommonLoadResourcesByFileName.LoadSpriteByFileName(tower.fileNameForTowerSprite);

                Button button = cellTower.GetComponent<Button>();
                button.onClick.AddListener(() => OnClickTowerInGrid?.Invoke(tower.name));

                caheAddedTowers.Add(cellTower);
            }
        }
    }

    public void ClickOnClose()
    {
        panelSelectTower.SetActive(false);
    }
}
