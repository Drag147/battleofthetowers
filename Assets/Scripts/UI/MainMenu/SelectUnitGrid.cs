﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectUnitGrid : MonoBehaviour
{
    public int indexUnit;
    public Action<int, string> OnClickUnitInGrid;

    public Sprite deleteSprite;
    public GameObject panelSelectUnit;
    public GameObject cellUnitPrefab;
    public Transform contentTransform;

    private List<GameObject> addedUnits = new List<GameObject>();

    public void ClickOnUnitInMenu(int indexUnit, bool canDelete)
    {
        this.indexUnit = indexUnit;
        panelSelectUnit.SetActive(true);

        foreach (var unit in AllUnitsUtils.GetAllUnits())
        {
            GameObject cellUnit = Instantiate(cellUnitPrefab, contentTransform);
            Image image = cellUnit.GetComponent<Image>();
            image.sprite = CommonLoadResourcesByFileName.LoadSpriteByFileName(unit.fileNameImageIcon);

            Button button = cellUnit.GetComponent<Button>();
            button.onClick.AddListener(() => OnClickUnitInGrid?.Invoke(indexUnit, unit.name));

            addedUnits.Add(cellUnit);
        }

        if (canDelete)
        {
            GameObject deleteCellUnit = Instantiate(cellUnitPrefab, contentTransform);
            Image imageDel = deleteCellUnit.GetComponent<Image>();
            imageDel.sprite = deleteSprite;

            Button buttonDel = deleteCellUnit.GetComponent<Button>();
            buttonDel.onClick.AddListener(() => OnClickUnitInGrid?.Invoke(indexUnit, null));
            addedUnits.Add(deleteCellUnit);
        }
    }

    public void ClickOnClose()
    {
        panelSelectUnit.SetActive(false);

        foreach (var cellUnit in addedUnits)
        {
            Destroy(cellUnit);
        }
        addedUnits.Clear();
    }
}
