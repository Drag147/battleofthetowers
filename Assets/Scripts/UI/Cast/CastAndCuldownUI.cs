﻿using System;
using UnityEngine;
using UnityEngine.UI;
using static CastEntityHandler;
using static CuldownEntityHandler;

public class CastAndCuldownUI : MonoBehaviour
{
    public Action actionOnClickButton;

    public Image imageIcon;
    public Button buttonForClick;
    public Text textCostCast;
    public Text textCuldown;

    public void Awake()
    {
        if (buttonForClick == null)
        {
            buttonForClick = GetComponent<Button>();
        }
        buttonForClick.onClick.AddListener(OnClickButton);
        buttonForClick.interactable = false;
    }

    public void ShowCuldown(object sender, CuldownEventArgs culdownEventArgs)
    {
        textCuldown.text = culdownEventArgs.culdownLeft.ToString("N2") + " сек.";
        SetInteractable(false);
    }

    public void ShowCostCast(CastEntityEventArgs castEntityEventArgs)
    {
        textCostCast.text = castEntityEventArgs.costCast.ToString();
        SetInteractable(!castEntityEventArgs.isCuldown && castEntityEventArgs.manaNeededRight);
    }

    public void OnClickButton()
    {
        actionOnClickButton?.Invoke();
    }

    private void SetInteractable(bool value)
    {
        buttonForClick.interactable = value;
        if (imageIcon != null)
        {
            imageIcon.color = new Color(imageIcon.color.r, imageIcon.color.g, imageIcon.color.b, value ? 1f : 0.5f);
        }
    }
}
