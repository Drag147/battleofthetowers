﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ChangeSpeedGameController : MonoBehaviour
{
    public EventHandler<ChangedSpeedGameEventArgs> OnChangeSpeedGame;

    public class ChangedSpeedGameEventArgs
    {
        public bool isDefaultSpeedGame;
    }

    public bool isDefaultSpeedGame = true;
    public Sprite defaultSpeedImage;
    public Sprite fastSpeedImage;
    public Image changeSpeedButtonImage;

    public void ClickOnChangeSpeed()
    {
        isDefaultSpeedGame = !isDefaultSpeedGame;

        if (isDefaultSpeedGame)
        {
            changeSpeedButtonImage.sprite = defaultSpeedImage;
        }
        else
        {
            changeSpeedButtonImage.sprite = fastSpeedImage;
        }
        OnChangeSpeedGame?.Invoke(this, new ChangedSpeedGameEventArgs { isDefaultSpeedGame = isDefaultSpeedGame });
    }
}
