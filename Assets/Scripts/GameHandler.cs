﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using static ChangeSpeedGameController;
using static HpHandler;

public class GameHandler : MonoBehaviour
{
    public static GameHandler Instance { get; private set; }

    public GameState nowGameState = GameState.Playing;

    public EventHandler<EventArgs> OnGamePlaying;
    public EventHandler<EventArgs> OnGamePause;
    public EventHandler<EventArgs> OnEndGame;

    public MoveCamera moveCamera;
    public MapHandler mapHandler;
    public CreationUnitSpawners creationUnitSpawners;

    public ManaConfigurerSubscribes manaConfigurerUI;
    public SpecialConfigurerSubscribes specialConfigurerUI;
    public UnitConfigurerSubscribes unitConfigurerUI;

    public SliderBarUI hpBarLeftTower;
    public SliderBarUI hpBarRightTower;

    public PauseController pauseController;
    public ChangeSpeedGameController changeSpeedController;
    public GameOverController gameOverController;

    private readonly float valueDefaultGameSpeed = 1f;
    private readonly float valueFastGameSpeed = 2f;
    private float nowTimeScale = 1f;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        pauseController.OnPause += SetGamePause;
        pauseController.OnPlay += SetGamePlay;
        changeSpeedController.OnChangeSpeedGame += ChangeSpeedGame;
        gameOverController.OnRestart += RestartLevel;
        gameOverController.OnMainMenu += ToMainMenu;

        moveCamera.ConfigureCameraBounds(mapHandler.ConfigureMap(
            CommonLoadResourcesByFileName.LoadSpriteByFileName(PlayerPrefs.GetString(KeysForPlayerPrefs.KeyForMapBackgroundName, "Image/Background/1")),
            PlayerPrefs.GetFloat(KeysForPlayerPrefs.KeyForMapWidth, 500f)
            ),
            (SidePlayer)PlayerPrefs.GetInt(KeysForPlayerPrefs.KeyForSideForPlayer, 0)
        );

        Dictionary <TowerForBuild, GameObject> towers = mapHandler.SpawnTowerToMap(
            JsonUtility.FromJson<TowerInfo>(PlayerPrefs.GetString(KeysForPlayerPrefs.KeyForLeftTowerInfo)),
            JsonUtility.FromJson<TowerInfo>(PlayerPrefs.GetString(KeysForPlayerPrefs.KeyForRightTowerInfo))
            );

        if (towers.TryGetValue(TowerForBuild.LEFT, out GameObject towerLeft))
        {
            //Десериализуем юнитов левого игрока
            ConfigurateTowerAndPlayer(towerLeft, hpBarLeftTower,
                JsonUtility.FromJson<UnitsInfoConfig>(PlayerPrefs.GetString(KeysForPlayerPrefs.KeyForLeftUnitsInfoPlayer)).unitsUnfo,
                Vector2.right);
        }

        if (towers.TryGetValue(TowerForBuild.RIGHT, out GameObject towerRight))
        {
            //Десериализуем юнитов правого игрока
            ConfigurateTowerAndPlayer(towerRight, hpBarRightTower,
                JsonUtility.FromJson<UnitsInfoConfig>(PlayerPrefs.GetString(KeysForPlayerPrefs.KeyForRightUnitsInfoPlayer)).unitsUnfo,
                Vector2.left);
        }
    }

    public void ConfigurateTowerAndPlayer(GameObject tower, SliderBarUI hpBarTower, List<UnitInfo> unitInfosPlayer, Vector2 durectionForUnits)
    {
        if (tower.TryGetComponent(out HpHandler hp))
        {
            hpBarTower.SetMaxValue(hp.maxValueHp);
            hp.OnHpValueChange += (object sender, HpEventArgs args) =>
            {
                hpBarTower.SetValue(args.nowValueHp);
            };

            hp.OnZeroHp += GameOver;
        }

        unitInfosPlayer.Sort((UnitInfo left, UnitInfo right) =>
        {
            return left.costCast.CompareTo(right.costCast);
        });

        AbstractPlayerHandler playerHandler;
        tower.TryGetComponent(out playerHandler);

        List<GameObject> unitSpawnersLeft = creationUnitSpawners.CreateUnitSpawnerFromUnitInfo(unitInfosPlayer,
            tower.GetComponentInChildren<SpawnUnitPoint>(),
            playerHandler,
            durectionForUnits);

        if (playerHandler is PlayerHandler)
        {
            manaConfigurerUI.SubscribeManaButtonUI(playerHandler.GetManaHandler());
            specialConfigurerUI.SubscribeSpecialButtonUI(playerHandler.GetSpecialHandler());
            unitConfigurerUI.SubscribeUnitsButtonUI(unitSpawnersLeft, unitInfosPlayer);
        }
        if (playerHandler is BotPlayerHandler)
        {
            (playerHandler as BotPlayerHandler).difficalteBot = (DifficalteBot)PlayerPrefs.GetInt(KeysForPlayerPrefs.KeyForDifficaltBot, (int)DifficalteBot.MIDDLE);
        }

        playerHandler.ConfiguratePlayersHandlers(unitSpawnersLeft);
    }


    private void SetGamePause(object sender, EventArgs args)
    {
        nowTimeScale = Time.timeScale;
        Time.timeScale = 0;
        nowGameState = GameState.Pause;
        OnGamePause?.Invoke(this, EventArgs.Empty);
    }

    private void SetGamePlay(object sender, EventArgs args)
    {
        Time.timeScale = nowTimeScale;
        nowGameState = GameState.Playing;
        OnGamePlaying?.Invoke(this, EventArgs.Empty);
    }

    private void ChangeSpeedGame(object sender, ChangedSpeedGameEventArgs args)
    {
        if (args.isDefaultSpeedGame)
        {
            nowTimeScale = valueDefaultGameSpeed;
        }
        else
        {
            nowTimeScale = valueFastGameSpeed;
        }
        Time.timeScale = nowTimeScale;
    }


    private void GameOver(object sender, HpEventArgs hpEventArgs)
    {
        nowGameState = GameState.EndGame;
        OnEndGame?.Invoke(this, EventArgs.Empty);
        Time.timeScale = 0;

        if ((sender as HpHandler).gameObject.tag == "LeftPlayer")
        {
            gameOverController.ShowGameOverWindow("Правый игрок победил!!!");
        }

        else
        {
            gameOverController.ShowGameOverWindow("Левый игрок победил!!!");
        }
    }

    private void RestartLevel()
    {
        nowTimeScale = valueDefaultGameSpeed;
        Time.timeScale = valueDefaultGameSpeed;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void ToMainMenu()
    {
        nowTimeScale = valueDefaultGameSpeed;
        Time.timeScale = valueDefaultGameSpeed;
        SceneManager.LoadScene(0);
    }
}

public enum GameState
{
    Playing,
    Pause,
    EndGame
}
