﻿using System.Collections.Generic;
using UnityEngine;

public class MapHandler : MonoBehaviour
{
    public TowerBuilder towerBuilder;

    //Позиция левой башни
    public Vector2 positionLeftTower;
    //Позиция правой башни
    public Vector2 positionRightTower;
    //Значение Y для земли
    public float groundY;

    //значение Z
    public static readonly float ZValue = -5f;

    //Трансформ карты background
    public Transform transformBackground;
    //Sprite для background карты
    public SpriteRenderer background;
    //Основная камера
    public Camera mainCamera;
    //Высотай UI нижней панели. P.S. высчитать почему то не получается, неправильные значения выходят.
    public float bootomPanelHeight = 70f;
    //Длина карты. Картинка фона повторяется
    public float widthLevel = 1000f;

    private float _camHalfWidth;
    private float _camHalfHeight;

    void Awake()
    {
        //Считаем размеры ширины камеры
        float screenAspect = Screen.width / (float)Screen.height;
        _camHalfHeight = mainCamera.orthographicSize;
        _camHalfWidth = screenAspect * _camHalfHeight;
    }

    public float ConfigureMap(Sprite mapBackground, float widthMap)
    {
        background.sprite = mapBackground;
        background.size = new Vector2 { x = widthMap < _camHalfWidth*2 ? _camHalfWidth*2 : widthMap, y = (2 * mainCamera.orthographicSize) - bootomPanelHeight };
        transformBackground.position = new Vector3(-_camHalfWidth, mainCamera.orthographicSize - background.size.y, transformBackground.position.z);

        //Рассчитываем левые и правые края для башен и вычитаем 10 чтобы не была в притык к концу карты.
        //Ставим по Y 0, так как пока мы не знаем высоту башни.
        positionLeftTower = new Vector2 { x = -_camHalfWidth, y = 0f };
        positionRightTower = new Vector2 { x = -_camHalfWidth + background.size.x, y = 0f };
        return background.size.x;
    }

    public Dictionary<TowerForBuild, GameObject> SpawnTowerToMap(TowerInfo leftTower, TowerInfo rightTower)
    {
        Dictionary<TowerForBuild, GameObject> towers = new Dictionary<TowerForBuild, GameObject>();

        towers.Add(TowerForBuild.LEFT, SpawnTower(leftTower,
            new Vector2 { x = positionLeftTower.x + leftTower.sizeTower.x / 2, y = positionLeftTower.y + leftTower.sizeTower.y / 2 },
            TowerForBuild.LEFT));

        towers.Add(TowerForBuild.RIGHT, SpawnTower(rightTower,
            new Vector2 { x = positionRightTower.x - rightTower.sizeTower.x / 2, y = positionRightTower.y + rightTower.sizeTower.y / 2 },
            TowerForBuild.RIGHT));

        return towers;
    }

    private GameObject SpawnTower(TowerInfo towerInfo, Vector2 positionTower, TowerForBuild towerForBuild)
    {
        GameObject instanceTower = towerBuilder.BuildTower(towerInfo, towerForBuild);
        instanceTower.transform.SetParent(transform);
        instanceTower.transform.localPosition = new Vector3
        {
            x = positionTower.x,
            y = positionTower.y,
            z = ZValue
        };
        instanceTower.transform.position = new Vector3
        {
            x = instanceTower.transform.localPosition.x,
            y = instanceTower.transform.position.y,
            z = instanceTower.transform.position.z
        };
        instanceTower.GetComponentInChildren<AbstractSpecialSpawnHandler>().positionTowerPlayer = instanceTower.transform.position;

        return instanceTower;
    }
}
