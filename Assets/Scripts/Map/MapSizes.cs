﻿using System.Collections.Generic;
using System.Linq;

public class MapSizes
{
    public static Dictionary<int, float> widthMap = new Dictionary<int, float>
    {
        { 0, 500f },
        { 1, 800f },
        { 2, 1200f }
    };

    public static float GetWidthByNumber(int number)
    {
        if(widthMap.TryGetValue(number, out float res))
        {
            return res;
        }
        return 500f;
    }

    public static int GetNumberByWidth(float width)
    {
        return widthMap.FirstOrDefault(item => item.Value == width).Key;
    }
}
