﻿using UnityEngine;

public class KeysForPlayerPrefs : MonoBehaviour
{
    #region keys PlayerPrefs
    public readonly static string KeyForPlayerConfig = "PlayerConfig";

    public readonly static string KeyForLeftTowerInfo = "LeftTowerInfo";
    public readonly static string KeyForRightTowerInfo = "RightTowerInfo";

    public readonly static string KeyForLeftUnitsInfoPlayer = "LeftUnitsInfoPlayer";
    public readonly static string KeyForRightUnitsInfoPlayer = "RightUnitsInfoPlayer";


    public readonly static string KeyForMapBackgroundName = "MapBackgroundName";
    public readonly static string KeyForMapWidth = "MapWidth";

    public readonly static string KeyForDifficaltBot = "DifficaltBot";

    public readonly static string KeyForSideForPlayer = "SideForPlayer";
    #endregion
}
