﻿using System.Collections.Generic;
using UnityEngine;
using static CastEntityHandler;

public abstract class AbstractPlayerHandler : MonoBehaviour, IPlayerHandler
{
    public string tagEnemy { get; set; }
    public int layerEnemy { get; set; }

    //Основные обработчики для специального умения
    public AbstractSpecialSpawnHandler specialHandler;
    //Основной обработчик для маны
    public ManaHandler manaHandler;

    public abstract void ConfiguratePlayersHandlers(List<GameObject> unitSpawners);

    protected void CommonConfigCastHandlerEvents(CastEntityHandler castHandler, CuldownEntityHandler cullDownHandler)
    {
        castHandler.OnEntityCastedCostCheck += CastCostEntityCheck;
        if (cullDownHandler != null)
        {
            castHandler.OnEntityCastedCullDownCheck += cullDownHandler.CastEntityCuldownCheck;
        }

        castHandler.OnEntityCast += (object sender, CastEntityEventArgs args) => GetManaHandler().MinusMana(args.costCast);
        if (cullDownHandler != null)
        {
            castHandler.OnEntityCast += (object sender, CastEntityEventArgs args) => cullDownHandler.StartCuldown();
        }
    }

    public void CastCostEntityCheck(object sender, CastEntityEventArgs castEventArgs)
    {
        castEventArgs.manaRight = GetManaHandler().ManaCheck(castEventArgs.costCast);
    }

    public ManaHandler GetManaHandler()
    {
        return manaHandler;
    }

    public void SetManaHandler(ManaHandler manaHandler)
    {
        this.manaHandler = manaHandler;
    }

    public AbstractSpecialSpawnHandler GetSpecialHandler()
    {
        return specialHandler;
    }
    public void SetSpecialHandler(AbstractSpecialSpawnHandler specialHandler)
    {
        this.specialHandler = specialHandler;
    }
}

public enum SidePlayer
{
    LEFT,
    RIGHT
}