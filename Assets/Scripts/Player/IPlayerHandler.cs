﻿using System.Collections.Generic;
using UnityEngine;

public interface IPlayerHandler
{
    string tagEnemy { get; set; }
    int layerEnemy { get; set; }

    void ConfiguratePlayersHandlers(List<GameObject> unitSpawners);

    ManaHandler GetManaHandler();
    void SetManaHandler(ManaHandler manaHandler);

    AbstractSpecialSpawnHandler GetSpecialHandler();
    void SetSpecialHandler(AbstractSpecialSpawnHandler specialHandler);
}
