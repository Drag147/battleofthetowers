﻿using System.Collections.Generic;
using UnityEngine;
using static CastEntityHandler;
using static CuldownEntityHandler;
using static ManaHandler;

[SerializeField]
public class PlayerHandler : AbstractPlayerHandler
{ 

    public override void ConfiguratePlayersHandlers(List<GameObject> unitSpawners)
    {
        var specialCastHandler = specialHandler.GetComponent<CastEntityHandler>();
        var specialCullDownHandler = specialHandler.GetComponent<CuldownEntityHandler>();

        PlayerConfigCastHandlerEvents(specialCastHandler, specialCullDownHandler);

        foreach (GameObject unitSpawner in unitSpawners)
        {
            //получаем 2 компонента cast и culdown
            if (unitSpawner.TryGetComponent(out CastEntityHandler castHandler))
            {
                var culdownHandler = unitSpawner.GetComponent<CuldownEntityHandler>();
                PlayerConfigCastHandlerEvents(castHandler, culdownHandler);
            }
        }
    }

    protected void PlayerConfigCastHandlerEvents(CastEntityHandler castHandler, CuldownEntityHandler cullDownHandler)
    {
        CommonConfigCastHandlerEvents(castHandler, cullDownHandler);

        manaHandler.OnManaChange += (object sender, ManaEventArgs args) =>
        {
            CastEntityEventArgs castEntityArgs = new CastEntityEventArgs { costCast = castHandler.costCast };
            //Запускаем проверку на КД.
            castHandler.OnEntityCastedCullDownCheck?.Invoke(castHandler, castEntityArgs);

            if (!castEntityArgs.isCuldown)
            {
                //Если перезарядки нет, то запускаем проверку на стоимость.
                castHandler.OnEntityCastedCostCheck?.Invoke(castHandler, castEntityArgs);
                castEntityArgs.manaNeededRight = castEntityArgs.manaRight;
                castHandler.OnShowCostCastEntity?.Invoke(castHandler, castEntityArgs);
            }
        };

        if (cullDownHandler != null)
        {
            cullDownHandler.OnEntityEndCullDown += (object sender, CuldownEventArgs args) =>
            {
                castHandler.OnShowCostCastEntity?.Invoke(castHandler, new CastEntityEventArgs { costCast = castHandler.costCast });
            };
        }
    }
}
