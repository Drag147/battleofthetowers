﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static CastEntityHandler;
using static ManaHandler;

[SerializeField]
public class BotPlayerHandler : AbstractPlayerHandler
{
    public DifficalteBot difficalteBot;

    private readonly List<CastEntityHandler> mayBeCast = new List<CastEntityHandler>();
    private System.Random random = new System.Random();

    private IEnumerator RandomAction(float durationBot)
    {
        while (true)
        {
            int rand = random.Next(0, mayBeCast.Count + 1);

            if (rand >= mayBeCast.Count)
            {
                manaHandler.ImproveMana();
            }
            else
            {
                mayBeCast[rand].CastEntity();
            }

            yield return new WaitForSeconds(durationBot);
        }
    }

    public override void ConfiguratePlayersHandlers(List<GameObject> unitSpawners)
    {
        var specialCast = specialHandler.GetComponent<CastEntityHandler>();
        var specialCullDownHandler = specialHandler.GetComponent<CuldownEntityHandler>();

        PlayerConfigCastHandlerEvents(specialCast, specialCullDownHandler);

        mayBeCast.Add(specialCast);

        foreach (GameObject unitSpawner in unitSpawners)
        {
            //получаем 2 компонента cast и culdown
            if (unitSpawner.TryGetComponent(out CastEntityHandler castHandler))
            {
                mayBeCast.Add(castHandler);
                var culdownHandler = unitSpawner.GetComponent<CuldownEntityHandler>();
                PlayerConfigCastHandlerEvents(castHandler, culdownHandler);
            }
        }

        StartCoroutine(RandomAction(ResolveDifficalteBot()));
    }

    protected void PlayerConfigCastHandlerEvents(CastEntityHandler castHandler, CuldownEntityHandler cullDownHandler)
    {
        CommonConfigCastHandlerEvents(castHandler, cullDownHandler);

        manaHandler.OnManaChange += (object sender, ManaEventArgs args) =>
        {
            CastEntityEventArgs castEntityArgs = new CastEntityEventArgs { costCast = castHandler.costCast };
            //Запускаем проверку на КД.
            castHandler.OnEntityCastedCullDownCheck?.Invoke(castHandler, castEntityArgs);

            if (!castEntityArgs.isCuldown)
            {
                //Если перезарядки нет, то запускаем проверку на стоимость.
                castHandler.OnEntityCastedCostCheck?.Invoke(castHandler, castEntityArgs);
                castEntityArgs.manaNeededRight = castEntityArgs.manaRight;
            }
        };
    }

    private float ResolveDifficalteBot()
    {
        switch (difficalteBot)
        {
            case DifficalteBot.EASY:
                return 2f;
            case DifficalteBot.MIDDLE:
                return 1.5f;
            case DifficalteBot.HARD:
                return 0.25f;
            default:
                return 2f;
        }
    }
}

public enum DifficalteBot
{
    EASY,
    MIDDLE,
    HARD
}
