﻿using System;
using UnityEngine;

public class PlayerBuilder : MonoBehaviour
{
    public IPlayerHandler BuildPlayerHandler(GameObject tower, TypePlayer typePlayer)
    {
        IPlayerHandler playerHandler = ResolvePlayerType(tower, typePlayer);
        return playerHandler;
    }

    public PlayerHandler GetComponentPlayerHandler(GameObject tower)
    {
        return tower.AddComponent<PlayerHandler>();
    }

    public BotPlayerHandler GetComponentBotPlayerHandler(GameObject tower)
    {
        return tower.AddComponent<BotPlayerHandler>();
    }

    public IPlayerHandler ResolvePlayerType(GameObject tower, TypePlayer typePlayer)
    {
        switch (typePlayer)
        {
            case TypePlayer.PLAYER:
                return GetComponentPlayerHandler(tower);
            case TypePlayer.BOT:
                return GetComponentBotPlayerHandler(tower);
            default:
                throw new ArgumentException("Illegal typePlayer value, " + typePlayer);
        }
    }
}
