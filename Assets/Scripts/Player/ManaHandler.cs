﻿using System;
using System.Collections;
using UnityEngine;

[SerializeField]
public class ManaHandler : MonoBehaviour
{
    //Текущее значение маны
    public int nowValue = 0;
    //Максимальное значение маны
    public int maxValue = 100;
    //Дельта которая прибавляется к maxValue для получения нового значения max при улучшении.
    public int improveDeltaMaxValue = 100;
    //Стоимость улучшения, равна Max/2
    public int imporoveCost = 50;
    //Сколько маны будет прибавляться за deltaTimeAddMana
    public int deltaPlusMana = 4;
    //Дельта которая прибавляется к deltaPlusMana при улучшении.
    public int imroveDeltaPlusMana = 1;
    //Через какой промежуток времени будет прибавляться мана
    public float deltaTimeAddMana = 0.3f;
    //Дельта вычитается при улучшении маны из времени прибавления
    public float imporveDeltaTimeAddMana = 0.015f;

    //Флаг того, что курутина не выполняется. Необходим для запуска курутины.
    //Так как курутина тормозит если текущее значение маны == макисмуму.
    public bool AddManaIsEndWork = true;

    public class ManaEventArgs : EventArgs
    {
        public int nowValue;
        public int maxValue;
        public int improveCost;
    }
    public EventHandler<ManaEventArgs> OnManaChange;

    void Start()
    {
        StartAddedMana();
    }

    public void StartAddedMana()
    {
        if (AddManaIsEndWork)
        {
            StartCoroutine(AddMana());
        }
    }

    IEnumerator AddMana()
    {
        //Ставим флаг что курутина выполняется.
        AddManaIsEndWork = false;
        //Проверяем состояние игры и текущее значение
        while (nowValue < maxValue)
        {
            //Зацикливаем значение nowValue.
            nowValue = Mathf.Clamp(nowValue + deltaPlusMana, 0, maxValue);
            //Запускаем ивент для отрисовки изменения в UI.
            OnManaChange?.Invoke(this, new ManaEventArgs { nowValue = nowValue, maxValue = maxValue, improveCost = imporoveCost });
            yield return new WaitForSeconds(deltaTimeAddMana);
        }
        //Ставим флаг что курутина закончила выполнение.
        AddManaIsEndWork = true;
    }

    //Минусуем ману, например при касте
    public bool MinusMana(int valueMinus)
    {
        if (nowValue >= valueMinus)
        {
            nowValue -= valueMinus;
            OnManaChange?.Invoke(this, new ManaEventArgs { nowValue = nowValue, maxValue = maxValue, improveCost = imporoveCost });
            StartAddedMana();
            return true;
        }
        return false;
    }

    //Проверка что текущего значения маны хватает для каста.
    public bool ManaCheck(int valueNeeded)
    {
        if (nowValue >= valueNeeded)
        {
            return true;
        }
        return false;
    }

    //Клик по мане. Улучшение 
    public void ImproveMana()
    {
        if (imporoveCost <= nowValue)
        {
            nowValue -= imporoveCost;
            maxValue += improveDeltaMaxValue;
            imporoveCost = maxValue / 2;
            deltaPlusMana += imroveDeltaPlusMana;
            deltaTimeAddMana -= imporveDeltaTimeAddMana;
            StartAddedMana();
        }
    }
}
