﻿using UnityEngine;

public class CommonLoadResourcesByFileName
{
    public static Sprite LoadSpriteByFileName(string fileName)
    {
        return Resources.Load<Sprite>(fileName);
    }

    public static GameObject LoadPrefabByFileName(string fileName)
    {
        return Resources.Load<GameObject>(fileName);
    }
}
