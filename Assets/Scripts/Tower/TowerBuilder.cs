﻿using System;
using UnityEngine;

public class TowerBuilder : MonoBehaviour
{
    public PlayerBuilder playerBuilder;
    public SpecialCastBuilder specialCastBuilder;
    public GameObject towerLeftPrefab;
    public GameObject towerRightPrefab;

    public GameObject BuildTower(TowerInfo towerInfo, TowerForBuild towerForBuild)
    {
        GameObject instanceTower = Instantiate(ResolvePrefab(towerForBuild));

        var spriteImage = CommonLoadResourcesByFileName.LoadSpriteByFileName(towerInfo.fileNameForTowerSprite);
        var spriteForTower = instanceTower.GetComponent<SpriteRenderer>();
        spriteForTower.sprite = spriteImage;
        spriteForTower.size = towerInfo.sizeTower;
        instanceTower.tag = towerInfo.tagAllias;
        instanceTower.layer = towerInfo.layerAllias;

        IPlayerHandler playerHandler = playerBuilder.BuildPlayerHandler(instanceTower, towerInfo.typePlayer);
        playerHandler.tagEnemy = towerInfo.tagEnemy;
        playerHandler.layerEnemy = towerInfo.layerEnemy;
        playerHandler.SetManaHandler(instanceTower.GetComponent<ManaHandler>());

        GameObject specialHandler = specialCastBuilder.BuildSpecialSpawn(instanceTower, towerInfo, towerForBuild);
        if(specialHandler.TryGetComponent(out AbstractSpecialSpawnHandler abstractSpecial))
        {
            playerHandler.SetSpecialHandler(abstractSpecial);
        }

        return instanceTower;
    }

    public GameObject ResolvePrefab(TowerForBuild towerForBuild)
    {
        switch (towerForBuild)
        {
            case TowerForBuild.LEFT:
                return towerLeftPrefab;
            case TowerForBuild.RIGHT:
                return towerRightPrefab;
            default:
                throw new ArgumentException("Invalid towerForBuild - " + towerForBuild);
        }
    }
}

public enum TowerForBuild
{
    LEFT,
    RIGHT
}
