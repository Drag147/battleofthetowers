﻿
using System;
using System.Collections.Generic;
using UnityEngine;

public class AllTowerUtils
{
    public static Dictionary<string, TowerInfo> allTowers = new Dictionary<string, TowerInfo>
    {
        {
            "StoneTower",
            new TowerInfo
            {
                name = "StoneTower",
                rusName = "Каменная башня",
                description = "Каменная башня",
                healthTower = 500,
                sizeTower = new Vector2(100, 150),
                fileNameForTowerSprite = "Image/Towers/StoneTower",

                specialCastInfo = AllSpecial.GetSpecialCastByName("FireBall")
            }
        },
        {
            "WoodTower",
            new TowerInfo
            {
                name = "WoodTower",
                rusName = "Деревянная башня",
                description = "Деревянная башня",
                healthTower = 500,
                sizeTower = new Vector2(100, 150),
                fileNameForTowerSprite = "Image/Towers/WoodTower",

                specialCastInfo = AllSpecial.GetSpecialCastByName("FireBall")
            }
        }
    };

    public static TowerInfo GetTowerByName(string name)
    {
        TowerInfo res;
        if (allTowers.TryGetValue(name, out res))
        {
            return res.Clone();
        }
        throw new ArgumentException("Inavalid name tower: " + name);
    }
}
