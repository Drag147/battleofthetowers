﻿using System;
using UnityEngine;

[Serializable]
public class TowerInfo
{
    public string name;
    public string rusName;
    public string description;
    public int healthTower;
    public string tagAllias;
    public int layerAllias;
    public string tagEnemy;
    public int layerEnemy;
    public string fileNameForTowerSprite;
    public Vector2 sizeTower;

    public TypePlayer typePlayer;

    public SpecialCastInfo specialCastInfo;

    public TowerInfo Clone()
    {
        return new TowerInfo
        {
            name = name,
            rusName = rusName,
            description = description,
            healthTower = healthTower,
            tagAllias = tagAllias,
            layerAllias = layerAllias,
            tagEnemy = tagEnemy,
            layerEnemy = layerEnemy,
            fileNameForTowerSprite = fileNameForTowerSprite,
            sizeTower = new Vector2(sizeTower.x, sizeTower.y),
            typePlayer = typePlayer,
            specialCastInfo = specialCastInfo.Clone()
        };
    }

    public static TowerInfo Clone(TowerInfo towerInfo)
    {
        return new TowerInfo
        {
            name = towerInfo.name,
            rusName = towerInfo.rusName,
            description = towerInfo.description,
            healthTower = towerInfo.healthTower,
            tagAllias = towerInfo.tagAllias,
            layerAllias = towerInfo.layerAllias,
            tagEnemy = towerInfo.tagEnemy,
            layerEnemy = towerInfo.layerEnemy,
            fileNameForTowerSprite = towerInfo.fileNameForTowerSprite,
            sizeTower = new Vector2(towerInfo.sizeTower.x, towerInfo.sizeTower.y),
            typePlayer = towerInfo.typePlayer,
            specialCastInfo = towerInfo.specialCastInfo.Clone()
        };
    }
}

public enum TypePlayer
{
    PLAYER,
    BOT
}
