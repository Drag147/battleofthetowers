﻿using System;
using UnityEngine;
using static CastEntityHandler;

public class CuldownEntityHandler : MonoBehaviour
{
    public class CuldownEventArgs : EventArgs
    {
        public float culdownTime;
        public float culdownLeft;
        public bool isCuldown;
    }

    //Ивент если время оставшейся перезарядки изменилось
    public EventHandler<CuldownEventArgs> OnCuldownLeftChange;
    //Ивент если КД закончилось
    public EventHandler<CuldownEventArgs> OnEntityEndCullDown;

    public float culdownTime;
    public float culdownLeft;
    public bool isCuldown;

    void Awake()
    {
        culdownLeft = culdownTime;
        isCuldown = false;
    }

    public void CastEntityCuldownCheck(object sender, CastEntityEventArgs castEntityEventArgs)
    {
        castEntityEventArgs.isCuldown = isCuldown;
    }

    public void StartCuldown()
    {
        culdownLeft = culdownTime;
        isCuldown = true;
    }

    private void Update()
    {
        if (isCuldown && culdownLeft > 0)
        {
            culdownLeft -= Time.deltaTime;
            OnCuldownLeftChange?.Invoke(this, new CuldownEventArgs { culdownTime = culdownTime, culdownLeft = culdownLeft, isCuldown = isCuldown });
            if (culdownLeft <= 0)
            {
                isCuldown = false;
                culdownLeft = 0;
                OnEntityEndCullDown?.Invoke(this, new CuldownEventArgs { culdownTime = culdownTime, culdownLeft = 0, isCuldown = isCuldown });
            }
        }
    }

}
