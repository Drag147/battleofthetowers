﻿using System;
using UnityEngine;

public class CastEntityHandler : MonoBehaviour
{
    public class CastEntityEventArgs : EventArgs
    {
        public int costCast;
        //Хватае маны для каста
        public bool manaNeededRight = false;
        public bool isCuldown = false;
        public bool manaRight = false;
    }

    //Ивент для проверки стоимости, запускается после проверки на перезарядку.
    public EventHandler<CastEntityEventArgs> OnEntityCastedCostCheck;
    //Ивент для проверки перезарядки.
    public EventHandler<CastEntityEventArgs> OnEntityCastedCullDownCheck;
    //Ивент каста сущности
    public EventHandler<CastEntityEventArgs> OnEntityCast;
    //Ивент отображения стоимости в UI
    public EventHandler<CastEntityEventArgs> OnShowCostCastEntity;

    //Стоимость вызова
    public int costCast;
    //То как объект кастуется, его GameObject.
    public MonoBehaviour gameObjectCast;
    //UI часть для вызова и отрисовки инфы
    public GameObject prefabForUICast;

    private void Start()
    {
        OnShowCostCastEntity?.Invoke(this, new CastEntityEventArgs { costCast = costCast });
    }

    //При клике на button.
    public void CastEntity()
    {
        CastEntityEventArgs castEntityArgs = new CastEntityEventArgs { costCast = costCast };
        //Запускаем проверку на КД.
        OnEntityCastedCullDownCheck?.Invoke(this, castEntityArgs);

        if (!castEntityArgs.isCuldown)
        {
            //Если перезарядки нет, то запускаем проверку на стоимость.
            OnEntityCastedCostCheck?.Invoke(this, castEntityArgs);
            if (castEntityArgs.manaRight)
            {
                //Все проверки прошли, делаем каст сущности.
                OnEntityCast?.Invoke(this, castEntityArgs);
                if (gameObjectCast != null)
                {
                    (gameObjectCast as IGameObjectCast).CastEntity();
                }
            }
        }
    }
}
