﻿using UnityEngine;

public class AnimationController : MonoBehaviour
{
    public Animator animator;

    public void Awake()
    {
        if (animator == null)
        {
            TryGetComponent(out animator);
        }
    }

    public void MoveAnimationPlay()
    {
        animator.SetBool("isMove", true);
    }

    public void AttackAnimationPlay()
    {
        animator.SetTrigger("isAttack");
    }

    public void IDLEAnimationPlay()
    {
        animator.SetBool("isMove", false);
    }

    public void ReciveDamageAnimationPlay()
    {
        animator.SetTrigger("isReciveDamage");
    }

    public void DyingAnimationPlay()
    {
        animator.SetBool("isDie", true);
        animator.SetTrigger("isDying");
    }
}
