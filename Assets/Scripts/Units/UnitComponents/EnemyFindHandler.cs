﻿using System;
using UnityEngine;

public class EnemyFindHandler : MonoBehaviour
{
    public EventHandler<EnemyFindEventArgs> OnCollisionEnemy;
    public EventHandler<EventArgs> OnEnemyNotFind;

    public class EnemyFindEventArgs : EventArgs
    {
        public Collider2D[] findedEnemysCollider;
        public Vector2 positionUnit;
    }

    public float rangeAttack;
    public int enemyLayer;
    public Vector2 pivotToCenterObject;

    private Transform _transform;

    public void Awake()
    {
        _transform = GetComponent<Transform>();
    }

    public void Update()
    {
        var positionWithPivot = new Vector2(_transform.position.x, _transform.position.y) + pivotToCenterObject;
        Collider2D[] findedColliders = Physics2D.OverlapCircleAll(positionWithPivot, rangeAttack, enemyLayer);
        Debug.DrawLine(positionWithPivot, new Vector2(positionWithPivot.x + rangeAttack, positionWithPivot.y), Color.green, 0.5f);
        if (findedColliders.Length > 0)
        {
            OnCollisionEnemy?.Invoke(this, new EnemyFindEventArgs { findedEnemysCollider = findedColliders, positionUnit = positionWithPivot });
        }
        else
        {
            OnEnemyNotFind?.Invoke(this, EventArgs.Empty);
        }
    }
}
