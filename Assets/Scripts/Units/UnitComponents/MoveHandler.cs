﻿using System;
using UnityEngine;

public class MoveHandler : MonoBehaviour
{
    public EventHandler<EventArgs> OnStop;
    public EventHandler<EventArgs> OnMove;

    public float moveSpeed;
    public Vector3 directionMove;
    public bool isMove = true;

    private Transform _transform;

    private void Awake()
    {
        _transform = transform;
    }

    private void Start()
    {
        Move();
    }

    public void Update()
    {
        if (isMove)
        {
            _transform.Translate(directionMove * moveSpeed * Time.deltaTime);
        }
    }

    public void Stop()
    {
        isMove = false;
        OnStop?.Invoke(this, EventArgs.Empty);
    }

    public void Move()
    {
        isMove = true;
        OnMove?.Invoke(this, EventArgs.Empty);
    }
}
