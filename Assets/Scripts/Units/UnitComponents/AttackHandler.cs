﻿using System;
using System.Collections;
using UnityEngine;
using static CheckEnemyIsMineHandler;

public class AttackHandler : MonoBehaviour
{
    public EventHandler<AttackEventArgs> OnAttack;
    public class AttackEventArgs : EventArgs
    {
        public float reciveDamage;
        public GameObject attackedUnit;
    }

    public float damage;
    public CuldownEntityHandler culdownAttack;

    public void Attack(object sender, CheckedEnemyEventArgs args)
    {
        if (args.checkedEnemy != null && args.checkedEnemy.TryGetComponent(out HpHandler hp))
        {
            if (culdownAttack != null)
            {
                if(culdownAttack.isCuldown)
                {
                    return;
                }
                culdownAttack.StartCuldown();
            }
            OnAttack?.Invoke(this, new AttackEventArgs { reciveDamage = damage, attackedUnit = hp.gameObject });
            StartCoroutine(DamageEntity(hp, 0.5f));
        }
    }

    public IEnumerator DamageEntity(HpHandler hpHandlerEntity, float duration)
    {
        yield return new WaitForSeconds(duration);
        hpHandlerEntity.ReciveDamage(damage);
    }
}
