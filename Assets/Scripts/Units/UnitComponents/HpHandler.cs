﻿using System;
using System.Collections;
using UnityEngine;

public class HpHandler : MonoBehaviour
{
    public class HpEventArgs : EventArgs
    {
        public float nowValueHp;
        public float damageValue = 0;
        public float maxValueHp;
    }

    public EventHandler<HpEventArgs> OnHpValueChange;
    public EventHandler<HpEventArgs> OnZeroHp;

    public float nowValueHp;
    public float maxValueHp;

    public void ReciveDamage(float damageValue)
    {
        nowValueHp = Mathf.Clamp(nowValueHp - damageValue, 0, maxValueHp);
        OnHpValueChange?.Invoke(this, new HpEventArgs
        {
            nowValueHp = nowValueHp,
            damageValue = damageValue,
            maxValueHp = maxValueHp
        });
        CheckHp();
    }

    public void CheckHp()
    {
        if (nowValueHp == 0)
        {
            //Временное решение
            GetComponent<BoxCollider2D>().enabled = false;
            if (TryGetComponent(out AttackHandler attack))
            {
                attack.enabled = false;
            }
            if (TryGetComponent(out MoveHandler move))
            {
                move.enabled = false;
            }
            if (TryGetComponent(out EnemyFindHandler finder))
            {
                finder.enabled = false;
            }
            enabled = false;

            StartCoroutine(DestroyGameObject(1.5f));
            OnZeroHp?.Invoke(this, new HpEventArgs
            {
                nowValueHp = nowValueHp,
                maxValueHp = maxValueHp
            });
        }
    }

    IEnumerator DestroyGameObject(float durationBeforeDead)
    {
        yield return new WaitForSeconds(durationBeforeDead);
        Destroy(gameObject);
    }
}
