﻿using System;
using UnityEngine;
using static EnemyFindHandler;

public class CheckEnemyIsMineHandler : MonoBehaviour
{
    public EventHandler<CheckedEnemyEventArgs> OnEnemyFind;
    public EventHandler<EventArgs> OnEnemyNotFind;
    public class CheckedEnemyEventArgs : EventArgs
    {
        public GameObject checkedEnemy;
    }

    public void CheckEnemy(object sender, EnemyFindEventArgs args)
    {
        var findedEnemys = args.findedEnemysCollider;

        var filtredEnemys = Array.FindAll(args.findedEnemysCollider, (Collider2D itemCheck) =>
        {
            //Если нет HP то и нет смысла атаковать.
            if (TryGetComponent(out CanAttackUnitsInfo canAttack)
                && itemCheck.TryGetComponent(out TypeUnitInfo typeUnitEnemy)
                && itemCheck.TryGetComponent(out HpHandler hpHandler))
            {
                return CheckTypes(canAttack.canAttack, typeUnitEnemy.typeUnit);
            }
            return false;
        });

        if (filtredEnemys.Length > 0) {
            GameObject result = FindFirstEnemyIsMine(filtredEnemys, new Vector2(args.positionUnit.x, args.positionUnit.y));
            OnEnemyFind?.Invoke(this, new CheckedEnemyEventArgs { checkedEnemy = result });
        }
        else
        {
            OnEnemyNotFind?.Invoke(this, EventArgs.Empty);
        }
    }

    private GameObject FindFirstEnemyIsMine(Collider2D[] enemys, Vector2 centerPosition)
    {
        if (enemys.Length > 1)
        {
            Array.Sort(enemys, (Collider2D left, Collider2D right) =>
            {
                var leftPos = left.gameObject.transform.position;
                var rightPos = right.gameObject.transform.position;

                float leftDistance = Vector2.Distance(leftPos, centerPosition);
                float rightDistance = Vector2.Distance(rightPos, centerPosition);

                return leftDistance.CompareTo(rightDistance);
            });
            return enemys[0].gameObject;
        }
        else if (enemys.Length == 1)
        {
            return enemys[0].gameObject;
        }
        throw new ArgumentException("Illegal enemys size, " + enemys.Length);
    }

    private bool CheckTypes(CanAttackUnits canAttackUnits, TypeUnit typeUnitEnemy)
    {
        if (canAttackUnits == CanAttackUnits.ALL)
        {
            return true;
        }
        if (canAttackUnits == CanAttackUnits.GROUND)
        {
            return typeUnitEnemy == TypeUnit.GROUND;
        }
        if (canAttackUnits == CanAttackUnits.FLY)
        {
            return typeUnitEnemy == TypeUnit.FLY;
        }
        return false;
    }
}
