﻿using System.Collections.Generic;
using UnityEngine;
using System;

public class CreationUnitSpawners : MonoBehaviour
{
    public Transform gameMapTransform;

    public static readonly string iconsUnitPath = "Image/IconsUnit/";
    private static readonly string prefabsUnitPath = "Prefabs/Unit/Units/";        

    public List<GameObject> CreateUnitSpawnerFromUnitInfo(List<UnitInfo> unitInfos,
        SpawnUnitPoint spawnUnitPoint,
        AbstractPlayerHandler playerHandler,
        Vector2 directionUnitMove)
    {
        List<GameObject> result = new List<GameObject>(unitInfos.Count);
        foreach (var unitInfo in unitInfos)
        {
            GameObject unitSpawner = new GameObject();
            unitSpawner.transform.SetParent(spawnUnitPoint.transform);
            unitSpawner.layer = playerHandler.gameObject.layer;

            var unitSpawnerComponent = unitSpawner.AddComponent<UnitSpawner>();
            unitSpawnerComponent.parentForUnit = gameMapTransform;
            unitSpawnerComponent.unitInfo = unitInfo;
            unitSpawnerComponent.unitPrefab = LoadPrefabByFileNameOrUnitName(unitInfo);

            float heightUnit = GetHeighUnit(unitSpawnerComponent.unitPrefab);
            ConfigurateSpawnPoint(unitSpawner, heightUnit, unitInfo.typeUnit);

            if (unitInfo.fileNameGameObjectPrefabForAmmunition != null)
            {
                unitSpawnerComponent.ammunitionPrefab = CommonLoadResourcesByFileName.LoadPrefabByFileName(unitInfo.fileNameGameObjectPrefabForAmmunition);
            }
            unitSpawnerComponent.tagForUnit = playerHandler.tag;
            unitSpawnerComponent.enemyLayer = 1 << playerHandler.layerEnemy;
            unitSpawnerComponent.directionMove = directionUnitMove;
            unitSpawnerComponent.flipSprite = directionUnitMove == Vector2.left ? true : false ;

            CastEntityHandler cast = unitSpawner.AddComponent<CastEntityHandler>();
            cast.costCast = unitInfo.costCast;
            cast.gameObjectCast = unitSpawnerComponent;

            CuldownEntityHandler culdown = unitSpawner.AddComponent<CuldownEntityHandler>();
            culdown.culdownTime = unitInfo.culdownCastTime;

            GameObject spawnEffect = Instantiate(CommonLoadResourcesByFileName.LoadPrefabByFileName(unitInfo.fileNameGameObjectPrefabForSpawnEffect), unitSpawner.transform);
            ConfigurateSpawnEffect(spawnEffect, heightUnit);
            unitSpawnerComponent.spawnEffect = spawnEffect.GetComponent<ParticleSystem>();

            result.Add(unitSpawner);
        }
        return result;
    }

    private float GetHeighUnit(GameObject unitPrefab)
    {
        if (unitPrefab.TryGetComponent(out SpriteRenderer sprite))
        {
            return sprite.size.y;
        }
        if (unitPrefab.TryGetComponent(out BoxCollider2D boxCollider))
        {
            return boxCollider.size.y;
        }
        throw new ArgumentException("Height could not be calculated");
    }

    private GameObject LoadPrefabByFileNameOrUnitName(UnitInfo unitInfo)
    {
        if (unitInfo.fileNameGameObjectPrefabForUnit == null || unitInfo.fileNameGameObjectPrefabForUnit.Length == 0)
        {
            return Resources.Load<GameObject>(prefabsUnitPath + unitInfo.name);
        }
        return CommonLoadResourcesByFileName.LoadPrefabByFileName(unitInfo.fileNameGameObjectPrefabForUnit);
    } 

    public static Sprite LoadSpriteByFileNameOrUnitName(UnitInfo unitInfo)
    {
        if (unitInfo.fileNameImageIcon == null || unitInfo.fileNameImageIcon.Length == 0)
        {
            return Resources.Load<Sprite>(iconsUnitPath + unitInfo.name);
        }
        return CommonLoadResourcesByFileName.LoadSpriteByFileName(unitInfo.fileNameImageIcon);
    }

    private void ConfigurateSpawnEffect(GameObject spawnEffect, float heightUnit)
    {
        Vector3 localScale = spawnEffect.transform.localScale;
        spawnEffect.transform.localScale = new Vector3(localScale.x, heightUnit * 2f, localScale.z);
    }

    private void ConfigurateSpawnPoint(GameObject unitSpawner, float heightUnit, TypeUnit typeUnit)
    {
        unitSpawner.transform.localPosition = Vector3.up * ((heightUnit / 2f) + ResolvePivotYByTypeUnit(typeUnit));
    }

    private float ResolvePivotYByTypeUnit(TypeUnit typeUnit)
    {
        switch (typeUnit)
        {
            case TypeUnit.GROUND:
                return 0f;
            case TypeUnit.FLY:
                return 50f;
            default:
                return 0f;
        }
    }
}
