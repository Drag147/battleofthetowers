﻿using System;
using UnityEngine;
using static AttackHandler;
using static CheckEnemyIsMineHandler;
using static HpHandler;

public class UnitSpawner : MonoBehaviour, IGameObjectCast
{
    public Transform parentForUnit;

    public ParticleSystem spawnEffect;
    public GameObject unitPrefab;
    public GameObject ammunitionPrefab;

    public UnitInfo unitInfo;

    public string tagForUnit;
    public int enemyLayer;
    public Vector2 directionMove;
    public bool flipSprite;

    private System.Random random = new System.Random();

    public void CastEntity()
    {
        GameObject unitGameObject = Instantiate(unitPrefab, parentForUnit);
        unitGameObject.tag = tagForUnit;
        unitGameObject.layer = LayerMask.NameToLayer(tagForUnit);

        if (unitGameObject.TryGetComponent(out SpriteRenderer spriteRenderer))
        {
            spriteRenderer.flipX = flipSprite;
        }

        var hp = HpComponentConfigurate(unitGameObject);
        var move = MoveComponentConfigurate(unitGameObject);
        var finder = FindEnemyComponentConfigurate(unitGameObject);
        var check = CheckFindedEnemyComponentConfigurate(unitGameObject);
        var canAttack = CanAttackUnitsComponentConfigurate(unitGameObject);
        var typeUnit = TypeUnitComponentConfigurate(unitGameObject);
        var culdown = CuldownAttackComponentConfigurate(unitGameObject);
        var attack = AttackComponentConfigurate(unitGameObject, culdown);

        //Выклюаем все компоненты с Update если умерли.
        hp.OnZeroHp += (object sender, HpEventArgs args) =>
        {
            EnableDisableComponents(new MonoBehaviour[] { move, finder, culdown }, false);
        };

        if (unitGameObject.TryGetComponent(out AnimationController animator))
        {
            finder.OnEnemyNotFind += (object sender, EventArgs args) =>
            {
                animator.MoveAnimationPlay();
            };
            check.OnEnemyNotFind += (object sender, EventArgs args) =>
            {
                animator.MoveAnimationPlay();
            };
            check.OnEnemyFind += (object sender, CheckedEnemyEventArgs args) =>
            {
                animator.IDLEAnimationPlay();
            };
            attack.OnAttack += (object sender, AttackEventArgs args) =>
            {
                animator.AttackAnimationPlay();
            };
            hp.OnZeroHp += (object sender, HpEventArgs args) =>
            {
                animator.DyingAnimationPlay();
            };
            hp.OnHpValueChange += (object sender, HpEventArgs args) =>
            {
                animator.ReciveDamageAnimationPlay();
            };
        }

        //Если юнит ренж и есть снаряд то настраиваем
        if (unitInfo.typeAttack == TypeAttack.RANGE && ammunitionPrefab != null)
        {
            attack.OnAttack += (object sender, AttackEventArgs args) =>
            {
                GameObject ammunition = Instantiate(ammunitionPrefab);
                ammunition.transform.position = (sender as AttackHandler).gameObject.transform.position;
                ammunition.transform.rotation = Quaternion.identity;
                if (ammunition.TryGetComponent(out ShellFlyToTargetPosition shellFly))
                {
                    shellFly.destanation = args.attackedUnit.transform;
                }
                ammunition.GetComponent<SpriteRenderer>().flipX = flipSprite;
            };
        }

        finder.OnCollisionEnemy += check.CheckEnemy;
        finder.OnEnemyNotFind += (object sender, EventArgs args) =>
        {
            move.Move();
        };
        check.OnEnemyFind += (object sender, CheckedEnemyEventArgs args) =>
        {
            move.Stop();
        };
        check.OnEnemyFind += attack.Attack;
        check.OnEnemyNotFind += (object sender, EventArgs args) =>
        {
            move.Move();
        };

        var hpBar = unitGameObject.GetComponentInChildren<SliderBarUI>();
        if (hpBar != null)
        {
            hpBar.SetMaxValue(hp.maxValueHp);
            hp.OnHpValueChange += (object sender, HpEventArgs args) =>
            {
                hpBar.SetValue(args.nowValueHp);
            };
        }

        SpawnUnitOnMap(unitGameObject);
        //Включаем компоненты с Update, юнит настроен целиком
        EnableDisableComponents(new MonoBehaviour[] { move, finder, culdown }, true);
    }

    private void EnableDisableComponents(MonoBehaviour[] componentForEnable, bool enableComponent)
    {
        foreach (var component in componentForEnable)
        {
            component.enabled = enableComponent;
        }
    }

    private void SpawnUnitOnMap(GameObject unitForSpawn)
    {
        if(spawnEffect != null)
        {
            spawnEffect.Play();
        }
        unitForSpawn.transform.position = new Vector3
        (
            transform.position.x,
            transform.position.y,
            //генерируем рандом, чтобы юниты не мерцали из-за одинакового z
            random.Next(-150, -45)
        );
    }

    private HpHandler HpComponentConfigurate(GameObject unit)
    {
        var hp = unit.AddComponent<HpHandler>();
        hp.maxValueHp = unitInfo.maxHp;
        hp.nowValueHp = unitInfo.maxHp;
        return hp;
    }

    private MoveHandler MoveComponentConfigurate(GameObject unit)
    {
        var move = unit.AddComponent<MoveHandler>();
        move.moveSpeed = unitInfo.speed;
        move.directionMove = directionMove;
        move.enabled = false;
        return move;
    }

    private EnemyFindHandler FindEnemyComponentConfigurate(GameObject unit)
    {
        var finderEnemy = unit.AddComponent<EnemyFindHandler>();
        finderEnemy.rangeAttack = unitInfo.rangeAttack;
        finderEnemy.enemyLayer = enemyLayer;
        finderEnemy.pivotToCenterObject = new Vector2 { x = 0, y = 0 };
        finderEnemy.enabled = false;
        return finderEnemy;
    }

    private CheckEnemyIsMineHandler CheckFindedEnemyComponentConfigurate(GameObject unit)
    {
        CheckEnemyIsMineHandler checkEnemyIsMineHandler = unit.AddComponent<CheckEnemyIsMineHandler>();
        return checkEnemyIsMineHandler;
    }

    private CanAttackUnitsInfo CanAttackUnitsComponentConfigurate(GameObject unit)
    {
        var canAttackUnits = unit.AddComponent<CanAttackUnitsInfo>();
        canAttackUnits.canAttack = unitInfo.canAttackUnits;
        return canAttackUnits;
    }

    private TypeUnitInfo TypeUnitComponentConfigurate(GameObject unit)
    {
        var typeUnit = unit.AddComponent<TypeUnitInfo>();
        typeUnit.typeUnit = unitInfo.typeUnit;
        return typeUnit;
    }

    private CuldownEntityHandler CuldownAttackComponentConfigurate(GameObject unit)
    {
        var culdown = unit.AddComponent<CuldownEntityHandler>();
        culdown.culdownTime = unitInfo.culdownAttackTime;
        culdown.enabled = false;
        return culdown;
    }

    private AttackHandler AttackComponentConfigurate(GameObject unit, CuldownEntityHandler culdownAttack)
    {
        var attack = unit.AddComponent<AttackHandler>();
        attack.damage = unitInfo.damage;
        attack.culdownAttack = culdownAttack;
        return attack;
    }
}
