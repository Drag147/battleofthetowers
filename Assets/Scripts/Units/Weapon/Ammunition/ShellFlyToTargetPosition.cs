﻿using UnityEngine;

public class ShellFlyToTargetPosition : MonoBehaviour
{
    public Vector3 prevDes;
    public Transform destanation;
    public float moveSpeed;

    public float distanceDestroy;

    private void Update()
    {
        Vector3 nowDest = prevDes;
        if (destanation != null)
        {
            prevDes = destanation.position;
        }
        if (Vector2.Distance(
            new Vector2 { x = transform.position.x, y = transform.position.y },
            new Vector2 { x = nowDest.x, y = transform.position.y }
            ) > distanceDestroy)
        {
            transform.Translate(Vector3.up * Time.deltaTime * moveSpeed);
            Vector3 relative = transform.InverseTransformPoint(nowDest);
            float angle = Mathf.Atan2(relative.x, relative.y) * Mathf.Rad2Deg;
            transform.Rotate(0, 0, -angle);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
