﻿using System;
using System.Collections.Generic;
using System.Linq;

public class AllUnitsUtils
{
    private static Dictionary<string, UnitInfo> allUnits = new Dictionary<string, UnitInfo>
    {
        {
            "ReaperGrey",
            new UnitInfo
            {
                name = "ReaperGrey",
                rusName = "Серый Жнец",
                description = "Серый Жнец",
                costCast = 125,
                culdownCastTime = 15f,
                maxHp = 100,
                speed = 15f,
                rangeAttack = 25f,
                damage = 25f,
                culdownAttackTime = 1.5f,
                typeUnit = TypeUnit.GROUND,
                canAttackUnits = CanAttackUnits.GROUND,
                typeAttack = TypeAttack.MELEE,
                fileNameImageIcon = "Image/IconsUnit/ReaperGrey",
                fileNameGameObjectPrefabForUnit = "Prefabs/Unit/Units/ReaperGrey/ReaperGrey",
                fileNameGameObjectPrefabForSpawnEffect = "Prefabs/Unit/Units/SpawnEffect"
            }
        },
        {
            "GolemGrey",
            new UnitInfo
            {
                name = "GolemGrey",
                rusName = "Серый Голем",
                description = "Серый Голем",
                costCast = 75,
                culdownCastTime = 10f,
                maxHp = 50,
                speed = 35f,
                rangeAttack = 25f,
                damage = 35f,
                culdownAttackTime = 2f,
                typeUnit = TypeUnit.GROUND,
                canAttackUnits = CanAttackUnits.GROUND,
                typeAttack = TypeAttack.MELEE,
                fileNameImageIcon = "Image/IconsUnit/GolemGrey",
                fileNameGameObjectPrefabForUnit = "Prefabs/Unit/Units/GolemGrey/GolemGrey",
                fileNameGameObjectPrefabForSpawnEffect = "Prefabs/Unit/Units/SpawnEffect"
            }
        },
        {
            "ElfGreen",
            new UnitInfo
            {
                name = "ElfGreen",
                rusName = "Зелёный Эльф",
                description = "Зелёный Эльф",
                costCast = 75,
                culdownCastTime = 15f,
                maxHp = 75,
                speed = 45f,
                rangeAttack = 250f,
                damage = 40f,
                culdownAttackTime = 2f,
                typeUnit = TypeUnit.GROUND,
                canAttackUnits = CanAttackUnits.ALL,
                typeAttack = TypeAttack.RANGE,
                fileNameImageIcon = "Image/IconsUnit/ElfGreen",
                fileNameGameObjectPrefabForUnit = "Prefabs/Unit/Units/ElfGreen/ElfGreen",
                fileNameGameObjectPrefabForAmmunition = "Prefabs/Unit/Units/ElfGreen/Ammunition/ArrowPrefab",
                fileNameGameObjectPrefabForSpawnEffect = "Prefabs/Unit/Units/SpawnEffect"
            }
        },
        {
            "FairyRed",
            new UnitInfo
            {
                name = "FairyRed",
                rusName = "Красная Фея",
                description = "Красная Фея",
                costCast = 75,
                culdownCastTime = 13f,
                maxHp = 100,
                speed = 35f,
                rangeAttack = 250f,
                damage = 40f,
                culdownAttackTime = 1.5f,
                typeUnit = TypeUnit.FLY,
                canAttackUnits = CanAttackUnits.ALL,
                typeAttack = TypeAttack.RANGE,
                fileNameImageIcon = "Image/IconsUnit/FairyRed",
                fileNameGameObjectPrefabForUnit = "Prefabs/Unit/Units/FairyRed/FairyRed",
                fileNameGameObjectPrefabForAmmunition = "Prefabs/Unit/Units/FairyRed/Ammunition/MagickBall",
                fileNameGameObjectPrefabForSpawnEffect = "Prefabs/Unit/Units/SpawnEffect"
            }
        }
    };

    public static List<UnitInfo> GetAllUnits()
    {
        var unitInfos = allUnits.Values.ToList();
        var res = new List<UnitInfo>(unitInfos.Count);
        res.AddRange(unitInfos.Select(unitInfo => unitInfo.Clone()));
        return res;
    }

    public static UnitsInfoConfig GetNowUnitsPlayer(List<string> armyPlayer)
    {
        var result = new List<UnitInfo>(armyPlayer.Count);

        foreach (var nameUnit in armyPlayer)
        {
            if(allUnits.TryGetValue(nameUnit, out var unit))
            {
                result.Add(unit.Clone());
            }
        }

        return new UnitsInfoConfig { unitsUnfo = result };
    }

    public static UnitInfo GetUnitByName(string name)
    {
        if(allUnits.TryGetValue(name, out var unit))
        {
            return unit.Clone();
        }
        throw new ArgumentException("Invalid unit name: " + name);
    }
}
