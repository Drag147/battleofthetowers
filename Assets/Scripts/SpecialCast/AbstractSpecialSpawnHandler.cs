﻿using UnityEngine;

public abstract class AbstractSpecialSpawnHandler : MonoBehaviour, IGameObjectCast
{
    public GameObject prefabForCast;

    public Vector2 positionTowerPlayer;

    public CanAttackUnits canAttack;

    //Тег для союзников
    public string tagForAllies;
    public string tagForEnemy;
    public float damage;

    public abstract void CastEntity();
}
