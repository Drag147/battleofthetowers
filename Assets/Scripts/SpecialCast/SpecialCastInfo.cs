﻿using System;
using System.Security.Cryptography;

[Serializable]
public class SpecialCastInfo
{
    public string name;
    public string rusName;
    public string description;

    public string fileNameSpecialCastSpawnPrefab;
    public TypeSpecialCast typeSpecialCast;

    public float culdown;
    public float damage;
    public int costCast;

    public SpecialCastInfo Clone()
    {
        return new SpecialCastInfo
        {
            name = name,
            rusName = rusName,
            description = description,

            fileNameSpecialCastSpawnPrefab = fileNameSpecialCastSpawnPrefab,

            typeSpecialCast = typeSpecialCast,
            culdown = culdown,
            damage = damage,
            costCast = costCast
        };
    }

    public static SpecialCastInfo Clone(SpecialCastInfo orig)
    {
        return new SpecialCastInfo
        {
            name = orig.name,
            rusName = orig.rusName,
            description = orig.description,

            fileNameSpecialCastSpawnPrefab = orig.fileNameSpecialCastSpawnPrefab,

            typeSpecialCast = orig.typeSpecialCast,
            culdown = orig.culdown,
            damage = orig.damage,
            costCast = orig.costCast
        };
    }
}

public enum TypeSpecialCast
{
    FIREBALL
}
