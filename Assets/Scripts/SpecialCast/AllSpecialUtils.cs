﻿using System;
using System.Collections.Generic;

public class AllSpecial
{
    public static Dictionary<string, SpecialCastInfo> allSpecial = new Dictionary<string, SpecialCastInfo>
    {
        { "FireBall", new SpecialCastInfo
            {
                name = "FireBall",
                rusName = "Фаерболл",
                description = "Выбирает ближайшую цель к башне",
                typeSpecialCast = TypeSpecialCast.FIREBALL,
                fileNameSpecialCastSpawnPrefab = "Prefabs/SpecialCast/FireBall/FireBallSpawn",
                costCast = 125,
                culdown = 35f,
                damage = 200
            }
        }
    };

    public static SpecialCastInfo GetSpecialCastByName(string name)
    {
        SpecialCastInfo special;
        if(allSpecial.TryGetValue(name, out special))
        {
            return special.Clone();
        }
        throw new ArgumentException("Invalid name special: " + name);
    }
}
