﻿using System;

[Serializable]
public class UnitInfo
{
    public string name;
    public string rusName;
    public string description;

    public float culdownCastTime;
    public int costCast;

    public int maxHp;

    public float culdownAttackTime;
    public float damage;
    public float rangeAttack;
    public float speed;

    public TypeUnit typeUnit;
    public TypeAttack typeAttack;
    public CanAttackUnits canAttackUnits;

    public string fileNameGameObjectPrefabForUnit;
    public string fileNameImageIcon;
    public string fileNameGameObjectPrefabForAmmunition;
    public string fileNameGameObjectPrefabForSpawnEffect;

    public UnitInfo Clone()
    {
        return new UnitInfo
        {
            name = name,
            rusName = rusName,
            description = description,
            culdownCastTime = culdownCastTime,
            costCast = costCast,
            maxHp = maxHp,
            culdownAttackTime = culdownAttackTime,
            damage = damage,
            rangeAttack = rangeAttack,
            speed = speed,
            typeUnit = typeUnit,
            typeAttack = typeAttack,
            canAttackUnits = canAttackUnits,
            fileNameGameObjectPrefabForUnit = fileNameGameObjectPrefabForUnit,
            fileNameImageIcon = fileNameImageIcon,
            fileNameGameObjectPrefabForAmmunition = fileNameGameObjectPrefabForAmmunition,
            fileNameGameObjectPrefabForSpawnEffect = fileNameGameObjectPrefabForSpawnEffect 
        };
    }

    public static UnitInfo Clone(UnitInfo orig)
    {
        return new UnitInfo
        {
            name = orig.name,
            rusName = orig.rusName,
            description = orig.description,
            culdownCastTime = orig.culdownCastTime,
            costCast = orig.costCast,
            maxHp = orig.maxHp,
            culdownAttackTime = orig.culdownAttackTime,
            damage = orig.damage,
            rangeAttack = orig.rangeAttack,
            speed = orig.speed,
            typeUnit = orig.typeUnit,
            typeAttack = orig.typeAttack,
            canAttackUnits = orig.canAttackUnits,
            fileNameGameObjectPrefabForUnit = orig.fileNameGameObjectPrefabForUnit,
            fileNameImageIcon = orig.fileNameImageIcon,
            fileNameGameObjectPrefabForAmmunition = orig.fileNameGameObjectPrefabForAmmunition,
            fileNameGameObjectPrefabForSpawnEffect = orig.fileNameGameObjectPrefabForSpawnEffect 
        };
    }
}

public enum TypeUnit
{
    GROUND,
    FLY
}

public enum TypeAttack
{
    MELEE,
    RANGE
}

public enum CanAttackUnits
{
    GROUND,
    FLY,
    ALL
}
