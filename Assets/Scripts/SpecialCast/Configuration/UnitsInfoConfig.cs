﻿using System;
using System.Collections.Generic;

[Serializable]
//Вспомогательный класс для сериализации юнитов, просто List не сериализируется, как оказалось)
public class UnitsInfoConfig
{
    public List<UnitInfo> unitsUnfo;
}
