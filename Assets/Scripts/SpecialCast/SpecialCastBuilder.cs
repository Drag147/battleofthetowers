﻿using UnityEngine;

public class SpecialCastBuilder : MonoBehaviour
{
    public GameObject BuildSpecialSpawn(GameObject tower, TowerInfo towerInfo, TowerForBuild towerForBuild)
    {
        GameObject instanceSpecialSpawn = Instantiate(CommonLoadResourcesByFileName.LoadPrefabByFileName(towerInfo.specialCastInfo.fileNameSpecialCastSpawnPrefab));
        instanceSpecialSpawn.transform.SetParent(tower.transform);
        SetPositionByTypeSpecial(instanceSpecialSpawn, towerInfo.specialCastInfo.typeSpecialCast, towerForBuild);

        if(instanceSpecialSpawn.TryGetComponent(out AbstractSpecialSpawnHandler abstractSpecialSpawnHandler))
        {
            abstractSpecialSpawnHandler.tagForAllies = towerInfo.tagAllias;
            abstractSpecialSpawnHandler.tagForEnemy = towerInfo.tagEnemy;
            abstractSpecialSpawnHandler.damage = towerInfo.specialCastInfo.damage;
            abstractSpecialSpawnHandler.positionTowerPlayer = new Vector2 { x = tower.transform.position.x, y = tower.transform.position.y };
        }
        if(instanceSpecialSpawn.TryGetComponent(out CuldownEntityHandler culdown))
        {
            culdown.culdownTime = towerInfo.specialCastInfo.culdown;
        }
        if(instanceSpecialSpawn.TryGetComponent(out CastEntityHandler castEntity))
        {
            castEntity.costCast = towerInfo.specialCastInfo.costCast;
        }

        return instanceSpecialSpawn;
    }

    public void SetPositionByTypeSpecial(GameObject specialInstance, TypeSpecialCast typeSpecialCast, TowerForBuild towerForBuild)
    {
        Vector2 positionSpawn = Vector2.zero;
        switch (typeSpecialCast)
        {
            case TypeSpecialCast.FIREBALL:
                positionSpawn = new Vector2(towerForBuild == TowerForBuild.LEFT ? -500 : 500, 500);
                break;
            default:
                break;
        }

        specialInstance.transform.position = positionSpawn;
    }
}
