﻿using UnityEngine;

public class FireBallDamageHandler : MonoBehaviour
{
    public string tagForEnemy;
    public float damageValue;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform != null && collision.transform.CompareTag(tagForEnemy))
        {
            collision.transform.GetComponent<HpHandler>().ReciveDamage(damageValue);
        }
    }
}
