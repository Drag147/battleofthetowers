﻿using System;
using UnityEngine;

public class FireBallHandler : AbstractSpecialSpawnHandler
{
    public override void CastEntity()
    {
        var filtredEnemyForAttack = FilterEnemyByCanAttack(GameObject.FindGameObjectsWithTag(tagForEnemy), canAttack);

        if (filtredEnemyForAttack.Length > 1)
        {
            Array.Sort(
                filtredEnemyForAttack,
                (GameObject left, GameObject right) =>
                {
                    float firstDistance = Vector2.Distance(positionTowerPlayer, new Vector2 { x = left.transform.position.x, y = left.transform.position.y });
                    float secondDistance = Vector2.Distance(positionTowerPlayer, new Vector2 { x = right.transform.position.x, y = right.transform.position.y });
                    return firstDistance.CompareTo(secondDistance);
                });
        }

        GameObject newFireBall = Instantiate(prefabForCast);
        newFireBall.transform.position = transform.position;
        //Находим самого ближайшего к базе врага и назначаем его в качестве цели.
        Transform transformEnemy = (filtredEnemyForAttack.GetValue(0) as GameObject).transform;
        newFireBall.GetComponent<ShellFlyToTargetPosition>().destanation = transformEnemy;
        FireBallDamageHandler fireBallDamage = newFireBall.GetComponent<FireBallDamageHandler>();
        fireBallDamage.damageValue = damage;
        fireBallDamage.tagForEnemy = tagForEnemy;
    }

    public GameObject[] FilterEnemyByCanAttack(GameObject[] enemys, CanAttackUnits canAttack)
    {
        if (canAttack == CanAttackUnits.ALL)
        {
            return enemys;
        }

        return Array.FindAll(enemys, (GameObject enemy) =>
        {
            if(enemy.TryGetComponent(out TypeUnitInfo typeUnit))
            {
                switch (canAttack)
                {
                    case CanAttackUnits.GROUND:
                        return typeUnit.typeUnit == TypeUnit.GROUND;
                    case CanAttackUnits.FLY:
                        return typeUnit.typeUnit == TypeUnit.FLY;
                    default:
                        return false;
                }
            }
            return false;
        });
    }
}
