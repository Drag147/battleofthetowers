﻿using System;
using System.Collections.Generic;
using System.Linq;

[Serializable]
public class PlayerConfig
{
    public string nameTower;
    public List<string> selectedArmyPlayer;

    public static PlayerConfig GetDefaultPlayerConfig()
    {
        Dictionary<string, TowerInfo>.KeyCollection keys = AllTowerUtils.allTowers.Keys;
        return new PlayerConfig
        {
            nameTower = keys.ElementAt(0),//new Random().Next(0, keys.Count)),
            selectedArmyPlayer = new List<string>
            {
                "GolemGrey",
                "ElfGreen"
            }
        };
    }
}
